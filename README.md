# Charge Owl

Overnight charging supervisor.
Designed to prevent overcharging the typically non-user-replaceable li-ion battery in a phone left to charge overnight.
Plugs in between your charging adapter and charging cable.

# Why?
It's because you're charging it wrong, obviously.

Many phone manufacturers nowadays choose to increase the perceived capacity of a (non-user-replaceable) battery by charging it up to around 4.35V instead of standard 4.2V, which typically lets them write a 10-20% bigger number in the specs (potentially turning a 3500mAh battery into 4000mAh+), with the extra "feature" of dramatically reducing the cycle life of said battery, making you more likely to buy a new phone more quickly.

For more scientific/technical information, please read the BatteryUniversity article BU-808:
https://batteryuniversity.com/index.php/learn/article/how_to_prolong_lithium_based_batteries

...and see for yourself if your phone overcharges its battery. 
This should ideally be done with a multimeter, but not everyone wants to (potentially destructively) open up their phone, especially if it's new and thus this device would be most beneficial. 
There are many apps that show the battery voltage with good accuracy. I personally like this one: https://f-droid.org/en/packages/com.kgurgul.cpuinfo/
(Charge Owl will never require any app to function and is designed to work on all devices that comply with the USB battery charging specification).

# How?
Since li-ion chargers reduce the battery charging current towards the end (possibly in part due to switching from C.C. to C.V. mode), monitoring the current drawn from a USB charger can be used to estimate the charge level of a particular phone while it's left charging.
Once the charging current drops below a set level, the charge is either limited (prevents waking up the screen at night) or terminated altogether, whatever you decide works best in your case.

Also, it disables quick charging by design, because you shouldn't use it unless you need it.
